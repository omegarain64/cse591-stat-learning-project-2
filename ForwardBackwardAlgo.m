function [ gamma, psi ] = ForwardBackwardAlgo( ObservationSeq,T )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    % Generate Diagonal Observation matrix (Assuming this can be generated from
    % Kmeans

    observationLength = size(ObservationSeq,1)+1;
    NumStates = size(ObservationSeq,2);

    % Forward
    forwardProb = ones(observationLength, NumStates)/NumStates;
    backwardProb = ones(observationLength, NumStates);

    for i = 2:observationLength
        % Forwards
        curObservation = diag(ObservationSeq(i-1,:));
        tempFowardProb = curObservation*T*forwardProb(i-1,:)';
        forwardProb(i, :) =  tempFowardProb./(sum(tempFowardProb));

        % Backward
        tempBackwardProb = T*curObservation*backwardProb(observationLength-i+2,:)';
        backwardProb(observationLength-i+1,:) =  tempBackwardProb./(sum(tempBackwardProb));
    end

    % Gamma and Psi
    gamma = zeros(NumStates, observationLength);
    for i = 1:observationLength
        for j = 1:NumStates
            gamma(j,i) = forwardProb(i,j)*backwardProb(i,j);
        end
        gamma(:,i) = gamma(:,i)/sum(gamma(:,i));
    end

    % Psi
    % psi =  zeros(observationLength, NumStates, NumStates);
    % TODO: CONSIDER TAKING NEXT OBSERVATION
    psi = zeros(NumStates,NumStates,observationLength-1);
    for i = 1:observationLength-2
        curPsi = zeros(NumStates);
        for j = 1:NumStates
            for k = 1:NumStates
                curPsi(j,k) = forwardProb(i,j)*T(j,k)*backwardProb(i+1,k)*ObservationSeq(i+1,k);
%                 curPsi(:,:)
            end
        end
        psi(:,:,i) = curPsi/sum(curPsi(:));
    end

end

