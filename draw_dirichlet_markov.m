function [tran_prob, init_prob]=draw_dirichlet_markov(mean_prob)
% draw the transition probability matrix
% each row is the probability vector from 1 state to the other states
mean_prob=mean_prob(:);
tran_prob=zeros(length(mean_prob));
kappa=eye(length(mean_prob))*sum(mean_prob);
for i=1: length(mean_prob)
    tran_prob(i, :)=randdirichlet(mean_prob+kappa(:, i))';
end
% add the self transition
init_prob=randdirichlet(mean_prob);
end
