% clear all
% matlabpool open
%% P2_1
alpha1 = [0.1, 0.2, 0.3, 0.2, 0.2];
alpha2 = [.5, 0.0, .4, .1, 0.0];
clear tran_prob init_prob
for i = 1:10
    [tran_prob{1,i}, init_prob{1,i}]=draw_dirichlet_markov(alpha1);
    [tran_prob{2,i}, init_prob{2,i}]=draw_dirichlet_markov(alpha2);
end

%% P2_2
numStateSequences = 30;
% numStateSequences = 1;
numStatesInSeq = 100;
% numStates = 10;
numAlpha =2;
numIter = 10;
for curAlpha = 1:numAlpha
    for curIter = 1:numIter
        curS = zeros(numStateSequences, numStatesInSeq); % state sequences
        for stateSeqIdx = 1:numStateSequences
            curS( stateSeqIdx, 1 ) = calcState( cell2mat( init_prob( curAlpha, curIter ) ) ); % Calc initial State
            for stateIdx = 2:numStatesInSeq
                distribution = cell2mat( tran_prob( curAlpha, curIter ) );
                lastState = curS( stateSeqIdx, stateIdx-1 );
                lastStateTransProb = distribution( lastState, : );
                curS( stateSeqIdx, stateIdx ) = calcState( lastStateTransProb );
            end
        end
        S{curAlpha,curIter} = curS;
    end
end
% S
% generate observations
for curAlpha = 1:numAlpha
    for curIter = 1:numIter
        curS = cell2mat(S(curAlpha,curIter));
        O{curAlpha,curIter} = normrnd(curS,0.3);
    end
end

%% P2_3 
numStates = 5;
% for curAlpha = 1:numAlpha
    curAlpha = 1
%     parfor curIter = 1:numIter
%     for curIter = 1:numIter
        curIter = 1
        Trans = ones(numStates)/numStates;
        curObservation = cell2mat(O(curAlpha,curIter));
        [~,centers] = kmeans(curObservation(:),numStates);
        centers = sort(centers);
        for stateSeqIdx = 1:numStateSequences
            probOfOinDist = getProb(curObservation(i,:), centers, 0.3);
            [Trans, init_prob,count] = BaumWelchAlgo(probOfOinDist, Trans);
        end
        Trans
        cell2mat( tran_prob(1,1))
%         estTransitions{curAlpha,curIter} = Trans;
%     end
% end
%% P2_4

