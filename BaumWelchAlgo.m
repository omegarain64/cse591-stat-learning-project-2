function [ TransitionMatrix, init_prob,count ] = BaumWelchAlgo( ObservationSeq,TransitionMatrix )
%     TransitionMatrix = ones(size(ObservationSeq,1))/size(ObservationSeq,1);
    newTransitionMatrix = zeros(size(TransitionMatrix));
    iterationStop = false;
    count = 0;
    while(~iterationStop)
        for i = 1:size(ObservationSeq,2)
            [gamma, psi] = ForwardBackwardAlgo(ObservationSeq', TransitionMatrix);
            sumPsi = sum(psi,3);
            sumGamma = sum(gamma,2);
            for i = 1:size(ObservationSeq,1)
                for j = 1:size(ObservationSeq,1)
                    newTransitionMatrix(i,j) = sumPsi(i,j)/sumGamma(i);
                end
            end
            % Normalize
            for i =1:size(ObservationSeq,1)
                newTransitionMatrix(i,:) = newTransitionMatrix(i,:)/sum(newTransitionMatrix(i,:));
            end
            %check for convergence
            difMat = abs(newTransitionMatrix - TransitionMatrix);
            if(max(difMat)<0.00001)
                iterationStop = true;
            end
            count = count+1;
            TransitionMatrix = newTransitionMatrix;
        end
    end 
    init_prob = gamma(1,:);
end

