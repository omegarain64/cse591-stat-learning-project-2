function [ probOfOinDist ] = getProb( observation, centers, std_Deviation )
    probOfOinDist = zeros(length(centers), length(observation));
    for i = 1:length(centers) %state
        for j=1:length(observation) % observation length
            probOfOinDist(i,j) = normpdf(observation(j), centers(i), std_Deviation);
        end
    end
    % Normalize
    sumPerObservation = sum(probOfOinDist,1);
    for i = 1:length(observation)
        probOfOinDist(:,i) = probOfOinDist(:,i)/sumPerObservation(i);
    end
end

